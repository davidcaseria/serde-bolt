serde-BOLT
==============

[![Crate](https://img.shields.io/crates/v/serde_bolt.svg?logo=rust)](https://crates.io/crates/serde_bolt)
[![Documentation](https://img.shields.io/static/v1?logo=read-the-docs&label=docs.rs&message=serde_bolt&color=informational)](https://docs.rs/serde_bolt/)
[![Safety Dance](https://img.shields.io/badge/unsafe-forbidden-success.svg)](https://github.com/rust-secure-code/safety-dance/)

An incomplete implementation of the Lightning BOLT message serialization format.  Compatible with rust-bitcoin `Encodable` and `Decodable` traits.

Unlike rust-bitcoin, the default is big-endian encoding and u16/u32 for length fields for the following types:

- Octets (u16 length field)
- Array (u16 length field)
- LargeOctets (u32 length field)
- IgnoredLargeOctets (u32 length field)

`Option` is implemented as a single byte, with `None` as `0x00` and `Some` as `0x01` followed by the value.

Domain-specific types are not implemented.  You can just use `Array<u8>` or `[u8; nnn]` or a wrapped version thereof.

Structs and tuples are considered transparent - they are not delimited in the stream.


Unimplemented
-------------

* TLVs

Usage
-----

```rust
    use hex::{encode, decode};
    extern crate alloc;

    use serde_bolt::{to_vec, from_vec, bitcoin::consensus::{Encodable, Decodable}, Array};
    use bitcoin_consensus_derive::{Encodable, Decodable};

    #[derive(Encodable, Decodable, PartialEq, Debug)]
    struct Thing([u8; 3]);
    
    #[derive(Encodable, Decodable, Debug)]
    struct Test {
        x: bool,
        a: u32,
        b: u8,
        c: Option<u16>,
        d: Option<u16>,
        e: Array<u8>,
        /// Sub-structs are transparent
        f: Thing
    }

    #[test]
    fn test_simple() {
        let test = Test {
            x: true,
            a: 65538, b:160, c: None, d: Some(3),
            e: vec![0x33, 0x44].into(),
            f: Thing([0x55, 0x66, 0x77])
        };
        let result = to_vec(&test).unwrap();
        assert_eq!("0100010002a00001000300023344556677", encode(&result));
    
        let decoded: Test = from_vec(&mut result.clone()).unwrap();
        assert_eq!(test.a, decoded.a);
		assert_eq!(test.f, decoded.f);
    }
```
